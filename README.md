# OpenGPU2020

#### 前言
本项目提供了一个支持openGL ES 1.1的开源GPU内核，命名为OpenGPU2020。

中国的GPU技术远远落后于美国。相比CPU这些年的进步，GPU技术在过去很多年中可谓是找不到北。造成这种状况的原因有可能是我国科研工作者一直以来对GPU不够重视，大学及科研院所极少有开辟图形学架构的课程。在AI技术的热潮中，利用GPU对AI算法进行训练已经成为最普遍的方法。至此GPU也终于不用在角落里吃灰，堂堂正正的走到了舞台的中央，在众多创业者/投资机构的追捧中成为了中国IC产业弯道超车美国的重要支撑。         

不过这一切与本项目想要提供的GPU内核完全无关。GPU诞生的目的就是为了更好的实现3D图形的渲染，也就是为了让游戏跑得更快，跑得更真实，而GPU多年的发展也始终没有背离这一初心。因此本项目选择实现一个早期的GPU，希望能够更细致的把图形流水线的功能和实现方法表达出来；也以此来致敬那个你方唱罢我登场，GPU演进波澜壮阔的岁月。

所谓万丈高楼平地起，希望本项目可以成为对GPU感兴趣的各位科研工作者/工程师/同学的基础，大家在此基础之上能够更好更快的理解GPU的基础功能以及实现逻辑，为中国GPU技术的发展乃至自重可控IC产业的发展添砖加瓦。


#### 特性
本项目所支持的OpenGL ES 1.1简化自OpenGL 1.5，支持视窗变换、定点染色、投影变换、图元装配、剪裁、背向面消隐、视窗变换、光栅化、纹理化、ROP功能。

![OpenGPU Architecture](https://gitee.com/graphichina/OpenGPU2020/raw/master/opengpu_arch.png)

#### 目录结构


   OpenGPU2020--|

                +--> rtl --+--> gc_ifdiv     ==> signed/unsigned/float pipeline divider

                |          |

                |          +--> pixel_shader ==> pixel shader

                |

                +--> inc                     ==> global include file

                |

                +--> vc                      ==> filelist

                |

                +--> tb                      ==> testbench

                |

                +--> sim                     ==> test enviroment


#### 交流联系

邮箱：tian.xiang@graphichina.com

微信公众号：

![Wechat](https://gitee.com/graphichina/OpenGPU2020/raw/master/wechat_pub.jpg)

交流微信群：

![WechatGroup](https://gitee.com/graphichina/OpenGPU2020/raw/master/gpu_group.jpg)

#### 修改记录

2020/5/31

>  首次发布，上传了pixel shader。
>
>  说明：完整的GPU项目尚未完成，此次更新为预览版本。

